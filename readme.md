

##Taylor Dahlke, 4/17/2019
Made using python 3.7 and openCV4.0.



#Instructions to run:

This project uses a Makefile to execute commands. 

1) You will need to first unzip the data:
**make unzipImages**

2) Then run to sort each folder into scenes:
**make sortScenesAllFolders**

Alternatively, you can just test on a single folder:
**make spectral_clustering1**

3) Then stitch the images in each discovered scene into composite images:
**make stitchScenesAllFolders**

Alternatively, you can just test on a single folder:
**make stitch_images1**


#Problem approach

In order to cluster images into separate scenes, it is first important to define what a 'scene' actually is. Each image in a scene will share keypoints with at least one other image in the actual scene. This means we can characterize the images as nodes on a graph, where the edges are the strength of the 'connection' to other images. I define this strength as the ratio of 'good' keypoints to total shared keypoints between two images. However, when the number of total shared keypoints is less than a certian threshold value, I treat the edge strength as zero. This gives me a directed graph.

I can use this graph to create an affinity matrix by averaging the graph with its transpose (done automatically in the code). By doing spectral clustering with this affinity matrix, I can identify the top K 'cliques' in the set of images. I can then split the set up into K cliques. If the metric I chose to define edge strength is an accurate measure of scene liklihood, then each image in a clique should be part of the same scene.

##Confidence of scene assignment
Information about how strongly associated an image is with a certain scene can also be extracted from the affinity matrix. Conductance (the ratio of edges that connect outside of a community over the total number of edges for a node) can give an idea of how likely that node is to be a part of that scene or 'community'. This information is printed out in each folder as **imageInfo.json**.


#Future work

1) When stitching the composite images together, if a lot of variation in the camera is present throughout the set of scene images, then some images may be warped a lot if the perspective that is chosen for the composite image is chosen poorly. With more time I would refine the composite image stitching to choose a perspective for the composite image that is always central to the set of images in order to avoid excessive image warping during homography.

2) Experimenting with other metrics of similarity (besides using the inlier ratio) to define the elements of the affinity matrix would be an interesting course of action to take.

3) Finding a better way to discriminate how many clusters to try and optimize for the k-means approach. Exploring non K-means clustering methods. Perhaps running the spectral clustering algorithm again on the new image set that has composite images would do a better job of combining spurious clusters that are easily recognized by a human as being part of the same scene.

4) Some images that are part of a scene may be connected to the main part of the scene by second order connections. Trying to better account for the higher-order linking between images may result in combining clusters more appropriately instead of having more small clusters.







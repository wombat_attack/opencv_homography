

import argparse
import numpy as np
import cv2
import os 
import gc



#-----------------------------------------------------
# Read in the command line arguments
parser=argparse.ArgumentParser()
parser.add_argument('--path', help='Folder path for folder containing images')
args=parser.parse_args()

# Get list of all files in directory
fullDirPath=os.path.realpath(args.path)
allfiles=os.listdir(fullDirPath)
allfiles = [(fullDirPath+'/'+file) for file in allfiles] 



for file in allfiles:
	image = cv2.imread(file) # queryImage
	imageDS1=cv2.pyrDown(image)
	imageDS2=cv2.pyrDown(imageDS1)
	cv2.imwrite(file, imageDS2)


gc.collect()






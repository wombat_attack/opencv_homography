

import argparse
import numpy as np
import cv2
import os 
import math
import random
import json
import gc
from sklearn.cluster import SpectralClustering



class SortImages(object):

	def __init__(self,cmdArgs):
		self.fullDirPath = os.path.realpath(cmdArgs.path)
		self.minMatch = int(cmdArgs.minMatch)
		self.blur_stencil = int(cmdArgs.blur_stencil)
		self.alpha = float(cmdArgs.alpha)
		self.verbose = bool(cmdArgs.verb)
		self.ransacVal = float(cmdArgs.ransacVal)
		return


	def test_match(self,queryPath,testing_files):

		closestImage = None

		# Loop over all the files in the folder
		for testPath in testing_files:

			print("----------------------------------------------")
			if (self.verbose): print(testPath)

			#-----------------------------------------------------
			# Run Feature detection on the two images
			MIN_MATCH_COUNT = self.minMatch

			self.queryImage_rgb = cv2.imread(queryPath) # queryImage
			self.queryImage = cv2.cvtColor(self.queryImage_rgb,cv2.COLOR_BGR2GRAY)
			# print(testPath)
			testImage_rgb = cv2.imread(testPath) # testImage
			testImage = cv2.cvtColor(testImage_rgb,cv2.COLOR_BGR2GRAY)

			# Blur the images to reduce the number of spurious features
			if (self.blur_stencil==0):
				queryImageG = self.queryImage
				testImageG = testImage
			else:
				queryImageG = cv2.GaussianBlur(self.queryImage,(self.blur_stencil,self.blur_stencil),0)
				testImageG = cv2.GaussianBlur(testImage,(self.blur_stencil,self.blur_stencil),0)


			# Split image1 into HSV
			hsv1 = cv2.cvtColor(self.queryImage_rgb, cv2.COLOR_BGR2HSV)
			h1,s1,v1 = cv2.split(hsv1)
			hsv_split1 = np.concatenate((h1,s1,v1),axis=1)
			cv2.imshow("Split HSV1",hsv_split1)

			# Split image2 into HSV
			hsv2 = cv2.cvtColor(testImage_rgb, cv2.COLOR_BGR2HSV)
			h2,s2,v2 = cv2.split(hsv2)
			hsv_split2 = np.concatenate((h2,s2,v2),axis=1)
			cv2.imshow("Split HSV2",hsv_split2)

			# Initiate SIFT detector
			sift = cv2.xfeatures2d.SIFT_create()

			# Find the keypoints and descriptors with SIFT from grayscale
			kp1, des1 = sift.detectAndCompute(queryImageG,None)
			kp2, des2 = sift.detectAndCompute(testImageG,None)

			# Find the keypoints and descriptors with SIFT from H
			kp1h, des1h = sift.detectAndCompute(h1,None)
			kp2h, des2h = sift.detectAndCompute(h2,None)

			# Find the keypoints and descriptors with SIFT from S
			kp1s, des1s = sift.detectAndCompute(s1,None)
			kp2s, des2s = sift.detectAndCompute(s2,None)

			des1=np.concatenate((des1,des1h,des1s),axis=0)
			des2=np.concatenate((des2,des2h,des2s),axis=0)

			kp1=kp1+kp1h+kp1s
			kp2=kp2+kp2h+kp2s

			# Match features between the two images (Brute Force)
			bf = cv2.BFMatcher()
			matches = bf.knnMatch(des1,des2,k=2)

			# Find good matches using Lowe's ratio test
			good = []
			for m,n in matches:
				if m.distance < self.alpha*n.distance:
					good.append(m)


			#-----------------------------------------------------
			# Transform the images to the same homography based on matched features

			print('%d / %d  matches/minMatches.' % (len(good),MIN_MATCH_COUNT))

			if len(good)>MIN_MATCH_COUNT:
				src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
				dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

				M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,self.ransacVal)
				matchesMask = mask.ravel().tolist()

				# How successful the transformation was
				if (self.verbose): print('%d / %d  inliers/matched.' % (np.sum(mask), len(mask)))

				inlierRatio = float(np.sum(mask)) / float(len(mask))

				if ( closestImage == None or inlierRatio > closestImage['inliers'] ):
					closestImage = {}
					closestImage['h'] = M
					closestImage['inliers'] = inlierRatio
					closestImage['path'] = testPath
					closestImage['rgb'] = testImage_rgb
					closestImage['img'] = testImage

				if (self.verbose): print("Closest Image: ", closestImage['path'])
				if (self.verbose): print("Closest Image Ratio: ", closestImage['inliers'])

			else:
				cmd = "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
				if (self.verbose): print(cmd)
				matchesMask = None

		return closestImage




	def findDimensions(self, image, homography):
	    base_p1 = np.ones(3, np.float32)
	    base_p2 = np.ones(3, np.float32)
	    base_p3 = np.ones(3, np.float32)
	    base_p4 = np.ones(3, np.float32)

	    (y, x) = image.shape[:2]

	    base_p1[:2] = [0,0]
	    base_p2[:2] = [x,0]
	    base_p3[:2] = [0,y]
	    base_p4[:2] = [x,y]

	    max_x = None
	    max_y = None
	    min_x = None
	    min_y = None

	    for pt in [base_p1, base_p2, base_p3, base_p4]:

	        hp = np.matrix(homography, np.float32) * np.matrix(pt, np.float32).T

	        hp_arr = np.array(hp, np.float32)

	        normal_pt = np.array([hp_arr[0]/hp_arr[2], hp_arr[1]/hp_arr[2]], np.float32)

	        if ( max_x == None or normal_pt[0,0] > max_x ):
	            max_x = normal_pt[0,0]

	        if ( max_y == None or normal_pt[1,0] > max_y ):
	            max_y = normal_pt[1,0]

	        if ( min_x == None or normal_pt[0,0] < min_x ):
	            min_x = normal_pt[0,0]

	        if ( min_y == None or normal_pt[1,0] < min_y ):
	            min_y = normal_pt[1,0]

	    min_x = min(0, min_x)
	    min_y = min(0, min_y)

	    return (min_x, min_y, max_x, max_y)




	def stitch_together(self,closestImage,compositePath):

		gc.collect()

		H = closestImage['h']
		H = H / H[2,2]
		H_inv = np.linalg.inv(H)


		(min_x, min_y, max_x, max_y) = self.findDimensions(closestImage['img'], H_inv)

		# Adjust max_x and max_y by base img size
		max_x = max(max_x, self.queryImage.shape[1])
		max_y = max(max_y, self.queryImage.shape[0])

		if (self.verbose): print("max_x = %d" % (max_x))
		if (self.verbose): print("max_y = %d" % (max_y))

		move_h = np.matrix(np.identity(3), np.float32)

		if ( min_x < 0 ):
			move_h[0,2] += -min_x
			max_x += -min_x

		if ( min_y < 0 ):
			move_h[1,2] += -min_y
			max_y += -min_y

		if (self.verbose): print("Homography: \n", H)
		if (self.verbose): print("Inverse Homography: \n", H_inv)
		if (self.verbose): print("Min Points: ", (min_x, min_y))

		mod_inv_h = move_h * H_inv

		img_w = int(math.ceil(max_x))
		img_h = int(math.ceil(max_y))

		gc.collect()

		if (self.verbose): print("New Dimensions: ", (img_w, img_h))

		# Crop edges
		if (self.verbose): print("Cropping...")
		base_h, base_w, base_d = self.queryImage_rgb.shape
		next_h, next_w, next_d = closestImage['rgb'].shape

		pixelcrop=0
		self.queryImage = self.queryImage[pixelcrop:(base_h-pixelcrop),pixelcrop:(base_w-5)]
		closestImage['rgb'] = closestImage['rgb'][pixelcrop:(next_h-pixelcrop),pixelcrop:(next_w-pixelcrop)]

		# Warp the new image given the homography from the old image
		base_img_warp = cv2.warpPerspective(self.queryImage_rgb, move_h, (img_w, img_h))
		if (self.verbose): print("Warped base image")

		next_img_warp = cv2.warpPerspective(closestImage['rgb'], mod_inv_h, (img_w, img_h))
		if (self.verbose): print("Warped next image")

		# Put the base image on an enlarged palette
		enlarged_base_img = np.zeros((img_h, img_w, 3), np.uint8)

		gc.collect()

		if (self.verbose): print("Enlarged Image Shape: ", enlarged_base_img.shape)
		if (self.verbose): print("Base Image Shape: ", self.queryImage.shape)
		if (self.verbose): print("Base Image Warp Shape: ", base_img_warp.shape)

		# Create masked composite
		(ret,data_map) = cv2.threshold(cv2.cvtColor(next_img_warp, cv2.COLOR_BGR2GRAY), 
		0, 255, cv2.THRESH_BINARY)

		# Add base image
		final_img = cv2.add(enlarged_base_img, base_img_warp, mask=np.bitwise_not(data_map), dtype=cv2.CV_8U)

		# Add next image
		final_img = cv2.add(final_img, next_img_warp, dtype=cv2.CV_8U)

		gc.collect()

		# output composite image
		print("Just merged image: %s" % (closestImage['path']))
		cv2.imwrite(compositePath, final_img)

		return compositePath,final_img




if __name__ == '__main__':

	#-----------------------------------------------------
	# Read in the command line arguments
	parser=argparse.ArgumentParser()
	parser.add_argument('--path', help='Folder path for folder containing images')
	parser.add_argument('--minMatch', help='Minimum number of matching reference points common to the two images')
	parser.add_argument('--blur_stencil', help='Stencil length for image Gaussian blurring',default=5)
	parser.add_argument('--alpha', help='Threshold for ignoring spurious feature matching',default=0.7)
	parser.add_argument('--verb', help='Whether to print out status during runtime',default=False)
	parser.add_argument('--output', help='Path to save the composite images to.')
	parser.add_argument('--ransacVal', help='Pixel distance of maximum reconstruction error for RANSAC algorithm as applied to fiding the homography matrix',default=3)
	parser.add_argument('--minInlierRatio', help='Minimum value of the inlier ratio (#inliers/#total_keypoints) used to determine reliability of of the homgraphy matrix. A higher value means more closest match images a rejected (and so less likely to stitch together two images)',default=0.5)
	parser.add_argument('--sceneList', help='JSON file with image file paths broken up into separate scenes.')


	args=parser.parse_args()
	outputPath = args.output
	sceneList = args.sceneList
	minMatch = int(args.minMatch)
	minInlierRatio = float(args.minInlierRatio)

	# Initialize sort image class
	stitch=SortImages(args)

	# Read in JSON file
	with open(sceneList) as f:
		d = json.load(f)


	sceneCount=0
	for cluster in d:
		image_pool = [file for file in cluster]

		sceneCount+=1
		sceneIncomplete=True
		pieces=outputPath.split('.')
		scene_output="%s_%d.%s" % (pieces[0],sceneCount,pieces[1])

		print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
		print("SCENE #%d"% (sceneCount))
		print(image_pool)


		keepGoing=True
		# Initialize the composite image as random image
		ri=random.randint(0, (len(image_pool)-1))
		compositePath=image_pool[ri]
		while (keepGoing):

			# Stitch images to the composite image
			match_file_path=compositePath
			print("Query image = %s" % (match_file_path))

			# Find remaining images
			image_pool = [file for file in image_pool if file != match_file_path]

			# Find the best image from remaining images to match query image
			closestImage = stitch.test_match(match_file_path,image_pool)

			# If the closest image is any good
			if (closestImage!=None):
				# Stitch them together to make a new composite image
				compositePath,newImg = stitch.stitch_together(closestImage,scene_output)
				# Remove the recently merged image from the image pool
				image_pool = [file for file in image_pool if file != closestImage['path']]
				# And add the new composite image to the pool
				image_pool.append(compositePath)
			else:
				image_pool.append(match_file_path)
				print("Couldnt find an image from pool that had more than %d raw keypoints. Trying another query image. Last query image put back into the image pool." % (minMatch))


			print("Remaining files =  %d" % (len(image_pool)))
			print(image_pool)
			if (len(image_pool)==1):
				keepGoing=False









			





import argparse
import numpy as np
import cv2
import os 
import math
import random
import json
import gc
import scipy
from sklearn.cluster import SpectralClustering
from scipy.sparse import csgraph
from sklearn.cluster import KMeans
from scipy.sparse.linalg import eigsh
import networkx as nx


def eigenDecomposition(A, plot = False):

	L = csgraph.laplacian(A, normed=True)
	n_components = A.shape[0]

	# LM parameter : Eigenvalues with largest magnitude (eigs, eigsh), that is, largest eigenvalues in 
	# the euclidean norm of complex numbers.
	eigenvalues, eigenvectors = eigsh(L, k=n_components, which="LM", sigma=1.0, maxiter=5000)

	# Identify the optimal number of clusters as the index corresponding
	# to the larger gap between eigen values
	index_largest_gap = np.argmax(np.diff(eigenvalues))
	nb_clusters = index_largest_gap + 1

	return nb_clusters, eigenvalues, eigenvectors


class SortImages(object):

	def __init__(self,cmdArgs):
		self.fullDirPath = os.path.realpath(cmdArgs.path)
		self.minMatch = int(cmdArgs.minMatch)
		self.blur_stencil = int(cmdArgs.blur_stencil)
		self.alpha = float(cmdArgs.alpha)
		self.verbose = bool(cmdArgs.verb)
		self.ransacVal = float(cmdArgs.ransacVal)
		return


	def inlierRatio(self,queryPath,test_file):

		print("----------------------------------------------")

		#-----------------------------------------------------
		# Run Feature detection on the two images
		MIN_MATCH_COUNT = self.minMatch

		self.queryImage_rgb_FULL = cv2.imread(queryPath) # queryImage
		self.queryImage_rgb1 = cv2.pyrDown(self.queryImage_rgb_FULL)
		self.queryImage_rgb = cv2.pyrDown(self.queryImage_rgb1)
		self.queryImage = cv2.cvtColor(self.queryImage_rgb,cv2.COLOR_BGR2GRAY)

		testImage_rgb_FULL = cv2.imread(test_file) # testImage
		testImage_rgb1 = cv2.pyrDown(testImage_rgb_FULL)
		testImage_rgb = cv2.pyrDown(testImage_rgb1)
		testImage = cv2.cvtColor(testImage_rgb,cv2.COLOR_BGR2GRAY)

		# Blur the images to reduce the number of spurious features
		if (self.blur_stencil==0):
			queryImageG = self.queryImage
			testImageG = testImage
		else:
			queryImageG = cv2.GaussianBlur(self.queryImage,(self.blur_stencil,self.blur_stencil),0)
			testImageG = cv2.GaussianBlur(testImage,(self.blur_stencil,self.blur_stencil),0)


		# Split image1 into HSV
		hsv1 = cv2.cvtColor(self.queryImage_rgb, cv2.COLOR_BGR2HSV)
		h1,s1,v1 = cv2.split(hsv1)
		hsv_split1 = np.concatenate((h1,s1,v1),axis=1)
		cv2.imshow("Split HSV1",hsv_split1)

		# Split image2 into HSV
		hsv2 = cv2.cvtColor(testImage_rgb, cv2.COLOR_BGR2HSV)
		h2,s2,v2 = cv2.split(hsv2)
		hsv_split2 = np.concatenate((h2,s2,v2),axis=1)
		cv2.imshow("Split HSV2",hsv_split2)


		# Initiate SIFT detector
		sift = cv2.xfeatures2d.SIFT_create()


		# Find the keypoints and descriptors with SIFT from grayscale
		kp1, des1 = sift.detectAndCompute(queryImageG,None)
		kp2, des2 = sift.detectAndCompute(testImageG,None)

		# # Find the keypoints and descriptors with SIFT from H
		# kp1h, des1h = sift.detectAndCompute(h1,None)
		# kp2h, des2h = sift.detectAndCompute(h2,None)

		# Find the keypoints and descriptors with SIFT from S
		kp1s, des1s = sift.detectAndCompute(s1,None)
		kp2s, des2s = sift.detectAndCompute(s2,None)

		# des1=np.concatenate((des1,des1h,des1s),axis=0)
		# des2=np.concatenate((des2,des2h,des2s),axis=0)

		# kp1=kp1+kp1h+kp1s
		# kp2=kp2+kp2h+kp2s

		des1=np.concatenate((des1,des1s),axis=0)
		des2=np.concatenate((des2,des2s),axis=0)

		kp1=kp1+kp1s
		kp2=kp2+kp2s




		# Match features between the two images (Brute Force) no need for FLANN 
		# (Fast Library for Approximate Nearest Neighbors )
		bf = cv2.BFMatcher()
		matches = bf.knnMatch(des1,des2,k=2)

		# Find good matches using Lowe's ratio test
		good = []
		for m,n in matches:
			if m.distance < self.alpha*n.distance:
				good.append(m)

		#-----------------------------------------------------
		# Transform the images to the same homography based on matched features

		if len(good)>MIN_MATCH_COUNT:
			src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
			dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

			M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,self.ransacVal)
			matchesMask = mask.ravel().tolist()

			# How successful the transformation was
			print('%d / %d  inliers/matched.' % (np.sum(mask), len(mask)))
			inlierRatio = float(np.sum(mask)) / float(len(mask))

		else:
			inlierRatio = 0.0


		return inlierRatio






if __name__ == '__main__':

	#-----------------------------------------------------
	# Read in the command line arguments
	parser=argparse.ArgumentParser()
	parser.add_argument('--path', help='Folder path for folder containing images')
	parser.add_argument('--minMatch', help='Minimum number of matching reference points common to the two images')
	parser.add_argument('--blur_stencil', help='Stencil length for image Gaussian blurring',default=5)
	parser.add_argument('--alpha', help='Threshold for ignoring spurious feature matching',default=0.7)
	parser.add_argument('--verb', help='Whether to print out status during runtime',default=False)
	parser.add_argument('--output', help='Path to save the composite images to.')
	parser.add_argument('--ransacVal', help='Pixel distance of maximum reconstruction error for RANSAC algorithm as applied to fiding the homography matrix',default=3)
	parser.add_argument('--minInlierRatio', help='Minimum value of the inlier ratio (#inliers/#total_keypoints) used to determine reliability of of the homgraphy matrix. A higher value means more closest match images a rejected (and so less likely to stitch together two images)',default=0.5)
	args=parser.parse_args()
	outputPath = args.path

	# Initialize sort image class
	findKeypoints=SortImages(args)

	# Get list of all files in directory
	verbose=args.verb
	fullDirPath=os.path.realpath(args.path)
	allfiles=os.listdir(fullDirPath)
	image_pool = [(fullDirPath+'/'+file) for file in allfiles]
	nfiles=len(allfiles)

	print("\n\nORIGINAL image pool =  %s \n\n" % (image_pool))
	print("Original image pool count =  %s]\n\n" % (len(image_pool)))

	# Make the affinity matrix
	affinity_matrix=np.zeros((len(image_pool),len(image_pool)))
	x=0
	for match_file_path in image_pool:
		y=0
		for test_file_path in image_pool:
			# Find the best image from remaining images to match query image
			ratio = findKeypoints.inlierRatio(match_file_path,test_file_path)
			# Assign value to the affinity matrix
			affinity_matrix[x,y] = ratio
			y=y+1
		x=x+1


	k, _,  _ = eigenDecomposition(affinity_matrix)
	print(f'Optimal number of clusters {k}')

	# Cluster the affinity matrix
	sc = SpectralClustering(k, affinity='precomputed', n_init=1000)
	sc.fit(affinity_matrix)

	# Assign image files based on clustering
	sceneList={}
	for i in range(0,(sc.labels_.size)):
		if int(sc.labels_[i]) in sceneList:
			sceneList[int(sc.labels_[i])].append(image_pool[i])
		else:
			sceneList[int(sc.labels_[i])]=[image_pool[i]]
		

	# Make cluster index list
	clusterList={}
	for i in range(0,(sc.labels_.size)):
		if int(sc.labels_[i]) in clusterList:
			clusterList[int(sc.labels_[i])].append(i)
		else:
			clusterList[int(sc.labels_[i])]=[i]

	if (verbose): print("clusterList =  %s" % (clusterList))


	# Write the scene clusters to json output
	dumpList=[]
	for key in sceneList:
		dumpList.append(sceneList[key])

	outputfile="%s/sceneList.json" % (outputPath)
	with open(outputfile, 'w') as outfile:  
	    json.dump(dumpList, outfile,indent=4)


	# Get conductance of each node to the other clusters
	# Output matrix: (nfiles x nclusters)
	conductance=np.zeros((affinity_matrix.shape[0],k))
	G=nx.from_numpy_matrix(affinity_matrix)
	imageInfo={}

	for i in range(0,affinity_matrix.shape[0]): # Loop over images
		S=[i] # Grab an image and make a list
		for c in range(0,k): # Loop over clusters
			T= clusterList[c]
			aa=nx.algorithms.cuts.conductance(G,S,T)
			conductance[i,c]=aa
		# Add to a dictionary for each image file
		imageInfo[image_pool[i]]=list(conductance[i,:])
	if (verbose): print(conductance)

	# Make JSON file that tells liklihood of image belonging to a certain cluster
	outputfile="%s/imageInfo.json" % (outputPath)
	with open(outputfile, 'w') as outfile:
		json.dump(imageInfo,outfile,indent=4)



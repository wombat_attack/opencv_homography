

cwd=$(shell pwd)
D=${cwd}/data
S=source


#=========================================================================
# Extract the images from the zip file
unzipImages:
	unzip dataset_1.zip -d ${D}

#=========================================================================
# Run spectral clustering script to divide images into scenes 
spectral_clustering%:
	rm -f ${D}/$*/compositeScene*.jpg;
	python ${S}/spectral_clustering.py --path ${D}/$* --minMatch 10 --blur_stencil 5 --alpha 0.7 \
	--ransacVal 5

sortScenesAllFolders:
	num2=1 ; while [[ $$num2 -le 9 ]] ; do \
		make spectral_clustering$$num2 ; \
		((num2 = num2 + 1)) ; \
	done

#=========================================================================
# Build composite images from the scene image clusters
stitch_images%:
	rm -f ${D}/$*/compositeScene*.jpg;
	python ${S}/stitch_images.py --path ${D}/$* --sceneList ${D}/$*/sceneList.json --minMatch 10 --blur_stencil 5 \
	--alpha 0.7 --output ${D}/$*/compositeScene.jpg --minInlierRatio 0.1 --ransacVal 10

stitchScenesAllFolders:
	num2=1 ; while [[ $$num2 -le 9 ]] ; do \
		make stitch_images$$num2 ; \
		((num2 = num2 + 1)) ; \
	done





#=========================================================================
# Optional ( for faster testing)
downsample_all_images: downsample_images1 downsample_images2 downsample_images3 \
downsample_images4 downsample_images5 downsample_images6 downsample_images7 \
downsample_images8 downsample_images9

downsample_images%:
	cp -rf ${D}/$* ${D}/$*orig;
	python ${S}/downsample_images.py --path ${D}/$*